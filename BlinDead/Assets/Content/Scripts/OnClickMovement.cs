using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickMovement : MonoBehaviour
{
    #region Variables
    RaycastHit Hit;
    Ray ray;

    GameObject currentTile = null;
    #endregion

    #region CallBacks
    void Start()
    {
        if (LevelManager.Instance.FirstTileplayer != null)
        {
            currentTile = LevelManager.Instance.SpawnerGeneric(LevelManager.Instance.FirstTileplayer,gameObject);
            LevelManager.Instance.CalculateNeighbours(currentTile);
        }   
        else
            Debug.Log("No tile returned");
    }

   
    void Update()
    {
        if(LevelManager.Instance.playerTurn)
            MovementOnClick();
    }
    #endregion

    #region UserFunctions
    void MovementOnClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out Hit, 10f))
            {
                if (Hit.transform.position != null && LevelManager.Instance.GetNeighbours.Contains(Hit.transform))
                {
                    gameObject.transform.position = new Vector3(Hit.transform.position.x, Hit.transform.position.y, -1f);
                    currentTile = Hit.transform.gameObject;
                    //GameObject.FindObjectOfType<LevelManager>().ReturnNeighbours(currentTile);
                    LevelManager.Instance.CalculateNeighbours(currentTile);
                 //   LevelManager.Instance.TurnReset();
                }
                else
                {
                    Debug.Log("Select nearby tiles only please!");
                }
            }
        }
    }

    //add random spawn points
    #endregion
}

