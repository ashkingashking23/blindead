using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance = null;

    #region Variables
  //  Ray ray;
    RaycastHit RayHit;
    List<Transform> Neighbours = null;              //work with same list for p2
    List<Transform> SpawnPointTiles = null;
    [SerializeField] List<Transform> PlayerSpawnPoints,EnemySpawnPoints;
    [SerializeField] float Offset = 60f,CurrentAngle;
    [SerializeField] public bool playerTurn = false,enemyTurn = true;

    Vector3 PositionOfRay;
    #endregion

    #region Callbacks
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Init();
        }
        else
            Destroy(gameObject);
        TurnReset();
    }
    #endregion

    #region UserFunctions
    void Init()
    {
        if (Neighbours == null)
            Neighbours = new List<Transform>();
        if (SpawnPointTiles == null)
            SpawnPointTiles = new List<Transform>();
    }

    public void TurnReset()
    {
        playerTurn = !playerTurn;
        enemyTurn = !enemyTurn;
    }

        
    public void CalculateNeighbours(GameObject CurrentTile)
    {
        if (CurrentTile != null)
        {
            Transform Temp;
            Neighbours.Clear();
            PositionOfRay = new Vector3(CurrentTile.transform.position.x, CurrentTile.transform.position.y, -.1f);
            for (CurrentAngle = 0f; CurrentAngle <= 300f; CurrentAngle += Offset)
            {
               // Debug.DrawRay(PositionOfRay, Quaternion.Euler(0, 0, CurrentAngle) * Vector3.right, Color.gray, 5f); for visual purpose
                if (Physics.Raycast(PositionOfRay, Quaternion.Euler(0, 0, CurrentAngle) * Vector3.right, out RayHit, 1f))
                {
                    Temp = RayHit.transform;
                    Neighbours.Add(Temp);
                }
            }
        }
    }


    public GameObject SpawnerGeneric(GameObject _obj,GameObject MainBody)
    {
        Transform temp;
        temp = _obj.gameObject.transform;
        Vector3 firstPosition = new Vector3(temp.position.x, temp.position.y, -1f);
        MainBody.transform.position = firstPosition;
        return temp.gameObject;
    }
    #endregion

    #region Properties
    public List<Transform> GetNeighbours
    {
        get { return Neighbours; }
    }

    public GameObject FirstTileplayer
    {
        get { return PlayerSpawnPoints[Random.Range(0, PlayerSpawnPoints.Count - 1)].gameObject; }
    }

    public GameObject FirstTileEnemy
    {
        get { return EnemySpawnPoints[Random.Range(0, EnemySpawnPoints.Count - 1)].gameObject; }
    }

    public static LevelManager Instance
    {
        get { return _instance; }
        private set { _instance = value; }
    }

    #endregion
}
