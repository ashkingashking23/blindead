using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    #region Variables
    GameObject currentTile;
    #endregion

    #region UnityCallbacks
    private void Start()
    {
        if (LevelManager.Instance.FirstTileEnemy != null)
        {
            currentTile = LevelManager.Instance.SpawnerGeneric(LevelManager.Instance.FirstTileEnemy,gameObject);
        //    LevelManager.Instance.CalculateNeighbours(currentTile);
        }
        else
            Debug.Log("No tile returned");
    }
    #endregion

    #region UserFunctions
    void EnemyTileMovement()
    {
        int randMovementFactor = Random.Range(1,3);
        switch (randMovementFactor)
        {
            case 1: MoveTowardsEnemy();
                break;
            case 2: MoveRandomly();
                break;
            default: return; 
        }
    }

    private void MoveRandomly()
    {
       
    }

    private void MoveTowardsEnemy()
    {
      
    }

    #endregion

}
